# Specyfikacja

Urządzenie **LATTICE_JTAG** jest programatorem JTAG/SPI do FPGA firmy Lattice (https://www.latticesemi.com/en/Products).

- Bazuje na układzie FTDI FT2232H.
- Interfejs USB 2.0 HS, gniazdo Micro B.
- Dwa interfejsy programowania: JTAG + SPI Master.
- Gniazda programowania:
  - IDC 2x5, raster 2.54 mm oraz 1.27 mm
  - Pinout zgodny z ARM Cortex
- Wybierane zworkami źródło napięcia VTRG zasilającego bufor JTAG/SPI:
  - Lokalne z programatora
  - Zdalne z podłączonego układu
- Regulowane zworkami lokalne napięcie VTRG: 1.2, 1.8, 2.5, 3.3 V.

# Zastosowania

Programator współpracuje z EVB [FPGA_EVB_M01_V1_0](https://gitlab.com/wojrus-projects/altium-designer/evb/fpga_evb_m01_v1_0).

Programator zawiera FT2232H więc może być używany (bez żadnych modyfikacji) z programem `JTAG Boundary Scanner` (https://github.com/viveris/jtag-boundary-scanner) do niskopoziomowej diagnostyki JTAG.

# Konfiguracja FTDI

Układ FTDI wymaga zapisania konfiguracji do EEPROM (U4 93LC56B).

Wymagania:
- Plik XML z konfiguracją (https://www.latticesemi.com/Products/DevelopmentBoardsAndKits/ProgrammingCablesforPCs, pobrać plik HW-USBN-2B-firmware.zip)
- Program FT_PROG (https://ftdichip.com)

Procedura:
- Z pliku ZIP wypakować `FTDI Firmware/HW-USBN-2B.xml`.
- Podłączyć LATTICE_JTAG (musi być włożona zworka JP1) i uruchomić FT_PROG.
- "Scan And Parse", program powinien znaleźć programator.
- "File" -> "Open Template", otworzyć plik HW-USBN-2B.xml.
- RMB na `"Device: 0 (Loc ID:0xXXXX)"` -> "Apply Template" -> "Template: HW-USBN-2B.xml".
- "Program Devices" -> "Program" -> "Close".
- Od teraz programator powinien mieć Product_Description = "Lattice HW-USBN-2B Ch" i powinien być wykrywany przez "Radiant Programmer" i "Lattice Diamond".

# Projekt PCB

Schemat: [doc/LATTICE_JTAG_V1_0_SCH.pdf](doc/LATTICE_JTAG_V1_0_SCH.pdf)

Widok 3D: [doc/LATTICE_JTAG_V1_0_3D.pdf](doc/LATTICE_JTAG_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

Rysunek montażowy: [doc/LATTICE_JTAG_V1_0_ASSEMBLY.pdf](doc/LATTICE_JTAG_V1_0_ASSEMBLY.pdf)

![](resource/pcb_3d.png "PCB 3D")

![](resource/pcb.png "PCB top")

# Licencja

MIT
